const addbtn = document.querySelector("#addbtn");
var inputvalue = document.querySelector("#inputitem");
var todolist = document.querySelector("#task_list");

var data_backup = {
    tasks_array: [

    ]
};

class item{
    constructor(itemName, status = false){
        this.createDiv(itemName, status);
    }

    createDiv(itemName, status){
        
        var removebtn = document.createElement("input");
        removebtn.setAttribute('type', "button");
        removebtn.value = "X";
        removebtn.classList.add("xbtn");
        removebtn.addEventListener("click", this.removeItem);

        var checkb = document.createElement("input");
        checkb.setAttribute("type", "checkbox");
        checkb.classList.add("cb");
        checkb.addEventListener("click", this.checkItem);
        checkb.checked = status;

        var taskcontent = document.createElement("div");
        taskcontent.classList.add("nonstr");
        taskcontent.innerHTML = itemName;

        var task = document.createElement("div");
        task.classList.add("task");

        task.appendChild(removebtn);
        task.appendChild(checkb);
        task.appendChild(taskcontent);

        todolist.appendChild(task);
        if (checkb.checked) {
            checkb.parentNode.childNodes[2].classList.add("strth");
            checkb.parentNode.childNodes[2].classList.remove("nonstr");
        }

    }

    removeItem(){
        // console.log(this);
        // console.log(this.parentNode);
        // console.log(this.parentNode.parentNode);
        this.parentNode.parentNode.removeChild(this.parentNode);
        backup();
    }

    checkItem(){

        backup();

        if(this.checked){
            this.parentNode.childNodes[2].classList.add("strth");
            this.parentNode.childNodes[2].classList.remove("nonstr");
        }
        else{
            this.parentNode.childNodes[2].classList.remove("strth");
            this.parentNode.childNodes[2].classList.add("nonstr");
        }
    }
}

function addItem(){

    new item(inputvalue.value);
    inputvalue.value = "";

    backup();
}



function clearList(){
    todolist.innerHTML = "";
    backup();
}


local_restore();

function backup(){
    data_backup.tasks_array = [];
    for(let i=0; i<todolist.childElementCount; i++){
        var taskName = todolist.childNodes[i].childNodes[2].innerHTML;
        var taskStatus = todolist.childNodes[i].childNodes[1].checked;

        var temp_json = {"id":i,"taskcontent":taskName , "status" : taskStatus};
        data_backup.tasks_array.push(temp_json);
    }
    console.log(data_backup);
    local_store();

}

function local_store(){
    localStorage.setItem('data',JSON.stringify(data_backup));
    console.log('local data' , localStorage.getItem('data'));
}

function local_restore(){
    
    if(localStorage.getItem("data")) {
        data_backup = JSON.parse(localStorage.getItem('data'));
        console.log(data_backup.tasks_array);

        for(let i=0;i<data_backup.tasks_array.length ;i++){
            new item(data_backup.tasks_array[i].taskcontent, data_backup.tasks_array[i].status);
        }
    }
}


